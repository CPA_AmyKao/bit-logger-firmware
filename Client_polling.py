#import modules
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import datetime
import json


class ModbusPolling:
    def __init__(self, name, port, ID):
        self.name = name
        self.port = port
        self.ID = ID
        self.config_setting = None
        self.rack_data = []
        self.mod_data = []
        self.client = ModbusClient()
        self.error_flag = 0
        self.num_module = 0
        self.time = datetime.datetime.now()
        self.load_config_file()

    def load_config_file(self):
        try:
            with open('/home/debian/BTL/Config/Product/LGChem_v1p9.json', 'r') as file:
                self.config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst)

    def update_config(self, name, port, ID):
        self.load_config_file()
        self.name = name
        self.port = port
        self.ID = ID
        self.client.close()
        self.init()

    def init(self):
        # Configuring Modbus client
        self.client = ModbusClient(
            method=self.config_setting["Connection"]["client_method"],
            port=self.port,
            baudrate=self.config_setting["Connection"]["client_baudrate"],
            timeout=self.config_setting["Connection"]["client_timeout"],
            parity=self.config_setting["Connection"]["client_parity"],
            stopbits=self.config_setting["Connection"]["client_stopbits"],
            bytesize=self.config_setting["Connection"]["client_bytesize"]
        )
        try:
            self.client.connect()
            try:
                # First time connecting to slave need to confirm number of battery modules there is
                temp = self.client.read_holding_registers(self.config_setting["Rack"]["Num_module"], 1, unit=self.ID)
                self.num_module = int(temp.registers[0])
                self.error_flag = 0
                print("Connection established to the battery")
                return
            except Exception as inst:
                print(inst)
                self.client.close()
                self.error_flag = 2
                return
        except Exception as inst:
            print(inst)
            self.client.close()
            self.error_flag = 1
            return

    def start_poll(self):
        self.rack_data.clear()
        self.mod_data.clear()

        try:
            if self.client.connect():
                #print("Modbus connected")
                # Load registers in temp variables
                temp_rack = self.client.read_holding_registers(self.config_setting["Rack"]["Address_start"], self.config_setting["Rack"]["Count"], unit=self.ID)
                #print("temp rack items")
                #print(temp_rack)
                try:
                    self.time = datetime.datetime.now()  # Timestamp of valid values collected
                    #print("rack loop------------------------------------------")

                    # Save registers value in array
                    for i in range(self.config_setting["Rack"]["Count"]):
                        self.rack_data.append(temp_rack.registers[i])

                        # Update any changes to number of modules
                        if i == 22 and (self.rack_data[i] != self.num_module):
                            self.num_module = self.rack_data[i]
                    #print(" Rack data ----------------------------------------")
                    #print(self.rack_data)

                except Exception as inst:
                    print(inst)
                    # Exception for no data within registers
                    #print("No Register readings")
                    self.client.close()
                    self.error_flag = 2
                    return
                try:
                    #print(" Mod loop -----------------------------------------")
                    for j in range(self.num_module):
                        temp_mod = self.client.read_holding_registers(self.config_setting["Module"]["Address_start"] + (j * self.config_setting["Module"]["Count"]), self.config_setting["Module"]["Count"], unit=self.ID)
                        for i in range(self.config_setting["Module"]["Count"]):
                            self.mod_data.append(temp_mod.registers[i])
                    #print(" Mod data -------------------------------------------")
                    #print(self.mod_data)
                    self.error_flag = 0
                    return
                except Exception as inst:
                    print(inst)
                    # Exception for no data within registers
                    #print("No Register readings")
                    self.client.close()
                    self.error_flag = 2
                    return
        except Exception as inst:
            print(inst)
            # Excpetion for not connecting to Modbus server
            #print("Not connected")
            self.client.close()
            self.error_flag = 1
            return

    def get_rack_values(self):
        return self.rack_data

    def get_mod_values(self):
        return self.mod_data
