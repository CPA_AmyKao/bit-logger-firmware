# import modules
import datetime
import json


class DataSelect:
    def __init__(self):
        self.server_config_setting = None
        self.client_config_setting = None
        self.load_config_file()
        self.server_data = []
        self.display_data = []
        self.current_time = datetime.datetime.now()

    def load_config_file(self):
        try:
            with open('/home/debian/BTL/Config/Product/LGChem_v1p9.json', 'r') as file:
                self.client_config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst.args)
            print(inst)
            return
        try:
            with open('/home/debian/BTL/Config/Product/Victron_can.json', 'r') as file:
                self.server_config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst.args)
            print(inst)
            return

    def select_data(self, data):
        self.server_data.clear()
        self.display_data.clear()
        # Use offset address of config file to determine which vales are for Victron Can
        for i in self.server_config_setting["LG_v1nine_offset"].values():
            self.server_data.append(data[i])
        for i in self.client_config_setting["Display_values_offset"].values():
            self.display_data.append(data[i])
        #print(self.server_data)
        #print(self.display_data)

    def get_server_data(self):
        return self.server_data

    def get_display_data(self):
        return self.display_data
