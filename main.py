#!/usr/bin/env python3
# import modules
from Client_polling import ModbusPolling as modbus
from Selector import DataSelect
from Battery_format import batteryFormat as formatter
from Controller import Controller as Controller
from Converter import Converter as Converter
from Server_transmit import CanSender as Sender
from Logger import DataLogging
from Syncing import myThread
import Web_gui
import json
import threading
import socket
import time
import datetime
import os

# User input
Battery_name = 'LG Chem'
Battery_port = '/dev/ttyS4'
Server_device = 'Victron_Can'
Server_port = 'can0'

# Initialised values
Battery_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
last_data_time = datetime.datetime.now()
last_logging_time = datetime.datetime.now()
loop = False
flag_bitLogger = 0

"""
client_error
1 - Modbus not connected
2 - No values in registers
1 - CAN not connected
2 - No alive message
"""
try:
    with open('/home/debian/BTL/Config/User/config.json', 'r') as file:
        user_config = json.load(file)
    file.close()
    # print(len(user_config))
    if len(user_config) == 6:
        user_config['Timezone'] = 'UCT'
        print("Added timezone")
except Exception as inst:
    print(inst)

print("UCT time:  " + str(time.strftime('%x %X %Z')))
os.environ['TZ'] = user_config['Timezone']
time.tzset()
print("Local time:  " + str(time.strftime('%x %X %Z')))
log_time = user_config["Frequency_of_logging"]
if log_time == 10:
    log_time = (log_time * 60) - 5
else:
    log_time = log_time * 60
# Initialising all the classes
client = modbus(Battery_name, Battery_port, user_config["Battery_slave_ID"])
client.init()
bat_format = formatter()
select = DataSelect()
controller = Controller()
convert = Converter()
server_send = Sender()

if client.num_module <= 0:
    log = DataLogging(user_config["Battery_number_of_modules"])
    print("number of mod register empty")
else:
    log = DataLogging(client.num_module)
    print("number of mod read from register")
    user_config["Battery_number_of_modules"] = client.num_module

# print(user_config)
try:
    with open('/home/debian/BTL/Config/User/config_tmp.json', 'w') as file:
        json.dump(user_config, file)
    file.close()
    os.rename('/home/debian/BTL/Config/User/config_tmp.json', '/home/debian/BTL/Config/User/config.json')
except Exception as inst:
    print(inst)

try:
    sync = myThread()
    sync.start()
except Exception as inst:
    print(inst)

try:
    ServerThread = threading.Thread(target=Web_gui.translator_server)
    ServerThread.daemon = True
    ServerThread.start()
except Exception as inst:
    print(inst)


def check_internet(host="8.8.8.8", port=53, timeout=1):
    """
    Check if there is internet connection.
    Code copied from: https://medium.com/better-programming/how-to-check-the-users-internet-connection-in-python-224e32d870c8
    :return: true/false
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except socket.error as ex:
        print(ex)
        return False


def main():
    global last_data_time, loop, last_logging_time, flag_bitLogger

    # Start polling for the client side
    client.start_poll()
    # print(client.get_rack_values())
    # print(client.get_mod_values())

    # If no error from the client side format client data
    if client.error_flag == 0:
        bat_format.format_battery_values(client.get_rack_values(), client.get_mod_values())  # Format battery value
        if bat_format.flag_eventChange:
            print("----------------------Event change -----------------------")
            log.log_data(bat_format.get_rack_values(), bat_format.get_mod_values(), flag_bitLogger)
        select.select_data(bat_format.get_rack_values())
        last_data_time = client.time
        loop = True  # Keep track of whether there is previous populated data for the client side
    # Restart Modbus client if there is an battery side error flag
    else:
        client.init()

    # Error handling when first time in loop with no populated data
    if loop:
        controller.charging_control(select.get_server_data(), client.error_flag, last_data_time)
        Web_gui.server_update_values(select.get_display_data(), check_internet())
    else:
        controller.charging_control(Battery_data, client.error_flag, last_data_time)
        Web_gui.server_update_values(Battery_data, check_internet())

    # Error handling on the server side. Only No error flags will the data be sent over the server channel
    # Only when the server side channel is active then convert the necessary server data
    if server_send.flag_error == 0:
        convert.convert(controller.get_data(), Battery_name)
        server_send.send_msg(convert.get_data())
    elif server_send.flag_error == 1:
        server_send.init()

    # Function that only log data per minute
    current_time = datetime.datetime.now()
    time_difference = current_time - log.timestamp
    flag_bitLogger = client.error_flag + (4 * server_send.flag_error) + (16 * sync.flag_RTCsync_error)

    if time_difference.seconds >= log_time:
        log.log_data(bat_format.get_rack_values(), bat_format.get_mod_values(), flag_bitLogger)


if __name__ == '__main__':
    print('------------- Interpreter starting -------------')
    print('------------- ' + str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + ' -------------')
    while True:
        main()
        time.sleep(1.6)
