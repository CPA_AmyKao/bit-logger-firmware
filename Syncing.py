import threading
import os
import datetime
import time
import json
import zipfile
import socket
import subprocess
# from systemd import journal
from File_rotation import FileRotation

with open('/home/debian/BTL/Config/User/config.json', 'r') as file:
    user_config = json.load(file)
file.close()

# What the remote Google drive is called
client_cloud = 'Client_remote'
CPA_cloud = 'CPA_remote'

destination_url_logs = '/' + str(user_config["Site_name"]) + '/Log'
destination_url_Firmware_update = 'BIT-Logger_Firmware_updatelog'
Home_Folder = "/home/debian/BTL/"
Log_Folder = "/home/debian/Logs"
Firmware_Folder = Home_Folder + "Firmware/"

Syslog_Folder = ""
logging_command = "sudo rclone copy -v " + Log_Folder + " " + client_cloud + ":" + destination_url_logs

check_update_command = "sudo rclone check --one-way " + CPA_cloud + ":/Firmware " + Firmware_Folder + " --log-file=/home/debian/updateslog.txt"
#check_update_command = "sudo rclone check --one-way " + CPA_cloud + ":/Firmwares " + Firmware_Folder + " --log-file=/home/debian/updateslog.txt"
check_update_log = "/home/debian/updateslog.txt"
donwload_update_command = "sudo rclone copy -v " + CPA_cloud + ":/Firmware " + Firmware_Folder
#donwload_update_command = "sudo rclone copy -v " + CPA_cloud + ":/Firmwares " + Firmware_Folder
reboot_command = "sudo reboot"

firmware_time = 0.5 # Hours
logging_time = user_config["Frequency_of_logs_sync"]  # Hours. Config file is in hours
syslog_time = 1

ALLOWED_EXTENSIONS = set(['zip'])
PASSWORD_ZIP = b'some_password'
check_filename = '0_Changelog'

files = FileRotation()


class myThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.firmware_version = "v1.3"
        self.push_log_time = datetime.datetime.now()
        self.pull_firmware_time = datetime.datetime.now()
        self.pull_syslog_time = datetime.datetime.now()
        self.time_network_reset = datetime.datetime.now()
        self.flag_need_to_update = False
        self.flag_reboot_required = False
        self.flag_firmwarelog_upload = False
        self.flag_pingstatus = False
        self.flag_RTCsync = False
        self.flag_RTCsync_error = False
        self.file_handling = False
        with open("/sys/bus/i2c/devices/0-0050/eeprom", "rb") as eeprom:
            header = eeprom.read(4)
            self.serial_num = eeprom.read(24)
        eeprom.close()
        with open('/root/hashfile.txt', 'r') as file:
            self.hash_num = file.readline()
        file.close()
        self.CPA_updatelog = str(self.serial_num, 'utf-8') + ".txt"
        self.client_updatelog = user_config["Site_name"] + ".txt"
        self.cmd_uploadLog_CPA = "sudo rclone copy -v /home/debian/" + self.CPA_updatelog + " " + CPA_cloud + ":Firmware_updates"
        self.cmd_uploadLog_Client = "sudo rclone copy -v /home/debian/" + self.client_updatelog + " " + client_cloud + ":" + destination_url_Firmware_update
        self.oldest_file = ''
        self.latest_file = ''

    def run(self):
        time.sleep(10)
        if self.check_internet():
            os.system(logging_command)

        while True:
            loop_time = datetime.datetime.now()

            # Check if it is time to check for new firmware updates
            if ((loop_time - self.pull_firmware_time) >= datetime.timedelta(hours=firmware_time)) and user_config[
                "Allow_remote_firmware_updates"] and not self.flag_reboot_required:
                # Remove all firmware in firmware folder is there is more then 1 firmware
                # This can indicate something went wrong in the process and better to restart the process over again
                if len(os.listdir(Firmware_Folder)) > 1:
                    for filename in os.listdir(Firmware_Folder):
                        os.remove(os.path.join(Firmware_Folder, filename))
                else:
                    # Only if there is internet connections then check and download new firmware
                    if self.check_internet():
                        # print("Internet Connection")
                        os.system(check_update_command)
                        self.check_update()
                        # Check if there is new firmware available
                        if self.flag_need_to_update:
                            self.flag_firmwarelog_upload = False  # Reset flag
                            print('Downloading latest firmware')
                            int_num_files = len(os.listdir(Firmware_Folder))  # Number of firmware files before download
                            os.system(donwload_update_command)
                            if len(os.listdir(Firmware_Folder)) > int_num_files:
                                if os.path.isfile(os.path.join("/home/debian", self.CPA_updatelog)):
                                    self.update_log("Firmware downloaded again")
                                    self.update_log_client("Firmware upgrade restarting")
                                else:
                                    self.create_firmware_log("Firmware downloaded")

                                # Determining which firmware is the latest firmware
                                try:
                                    self.oldest_file = ''
                                    self.latest_file = ''
                                    first_loop = True
                                    for filename in os.listdir(Firmware_Folder):
                                        if first_loop:
                                            self.oldest_file = filename
                                            self.latest_file = filename
                                            first_loop = False
                                            print("First loop oldest filename: " + str(self.oldest_file))
                                            print("First loop latest filename: " + str(self.latest_file))
                                        if os.stat(os.path.join(Firmware_Folder, filename))[8] > \
                                                os.stat(os.path.join(Firmware_Folder, self.latest_file))[8]:
                                            self.latest_file = filename
                                        if os.stat(os.path.join(Firmware_Folder, filename))[8] < \
                                                os.stat(os.path.join(Firmware_Folder, self.oldest_file))[8]:
                                            self.oldest_file = filename
                                    print("Oldest filename: " + str(self.oldest_file))
                                    print("Latest filename: " + str(self.latest_file))
                                except Exception as inst:
                                    print(inst.args)
                                    print(inst)

                                # Unzipping and updating the firmware
                                try:
                                    zip_ref = zipfile.ZipFile(os.path.join(Firmware_Folder, self.latest_file), 'r')
                                    # Extract latest firmware file
                                    for x in zip_ref.namelist():
                                        if x == check_filename:
                                            self.update_log("Firmware is recognised")
                                            try:
                                                zip_ref.extractall(path=Home_Folder, pwd=PASSWORD_ZIP)
                                                zip_ref.close()
                                                self.update_log("Firmware unzipped successful")
                                                self.flag_reboot_required = True
                                                self.pull_firmware_time = datetime.datetime.now()
                                                if len(os.listdir(Firmware_Folder)) > 1:
                                                    os.remove(os.path.join(Firmware_Folder, self.oldest_file))
                                                try:
                                                    os.system(
                                                        reboot_command)  # Reboot system for new firmware to take place
                                                except Exception as inst:
                                                    print(inst.args)
                                                    print(inst)
                                                    os.system("sudo reboot --force")
                                            except Exception as inst:
                                                self.update_log("Firmware cannot unzip")
                                                if os.path.exists(os.path.join(Firmware_Folder, self.latest_file)):
                                                    os.remove(os.path.join(Firmware_Folder, self.latest_file))
                                                self.pull_firmware_time = datetime.datetime.now()
                                                print(inst.args)
                                                print(inst)

                                    # If there was no recognisable file, delete firmware and try again
                                    if not self.flag_reboot_required:
                                        zip_ref.close()
                                        self.update_log("Firmware not recognised by program")
                                        if os.path.exists(os.path.join(Firmware_Folder, self.latest_file)):
                                            os.remove(os.path.join(Firmware_Folder, self.latest_file))
                                        self.pull_firmware_time = datetime.datetime.now()
                                except Exception as inst:
                                    print(inst.args)
                                    print(inst)
                                    zip_ref.close()
                                    self.update_log("Firmware not a zip file")
                                    if os.path.exists(os.path.join(Firmware_Folder, self.latest_file)):
                                        os.remove(os.path.join(Firmware_Folder, self.latest_file))
                                    self.pull_firmware_time = datetime.datetime.now()

                            else:
                                if os.path.isfile(os.path.join("/home/debian", self.CPA_updatelog)):
                                    self.update_log("Cannot download firmware")
                                else:
                                    self.create_firmware_log("Cannot download firmware")
                                self.pull_firmware_time = datetime.datetime.now()
                        else:
                            self.pull_firmware_time = datetime.datetime.now()
                    else:
                        self.pull_firmware_time = datetime.datetime.now()

            # Function to determine if the firmware was uploaded successfully and rebooted
            # elif os.path.isfile(os.path.join("/home/debian", self.CPA_updatelog)) and self.check_internet() and not self.flag_firmwarelog_upload:
            elif os.path.isfile(os.path.join("/home/debian", self.CPA_updatelog)) and self.check_internet() and not self.flag_firmwarelog_upload:
                print(self.cmd_uploadLog_CPA)
                with open(os.path.join("/home/debian", self.CPA_updatelog), 'r') as log:
                    data = log.read().splitlines()
                    last_line = data[-1]
                file.close()
                last_line = last_line.rsplit(" ---- ")
                print("Last line in CPA update log: " + str(last_line))
                if last_line[-1] == "Firmware unzipped successful" or last_line[-1] == "Reboot unsuccessful":
                    if self.flag_reboot_required:
                        self.update_log("Reboot unsuccessful")
                        self.update_log_client("Firmware uploaded BUT reboot unsuccessful. Rebooting again...")
                        os.system(self.cmd_uploadLog_CPA)
                        os.system(self.cmd_uploadLog_Client)
                        os.system("sudo reboot --force")
                    else:
                        self.update_log("Reboot Successful. New firmware version is: " + self.firmware_version)
                        self.update_log_client(
                            "Firmware uploaded and reboot successful. New firmware version is: " + self.firmware_version)
                        os.system(self.cmd_uploadLog_CPA)
                        os.system(self.cmd_uploadLog_Client)
                        os.remove(os.path.join("/home/debian", self.CPA_updatelog))
                        os.remove(os.path.join("/home/debian", self.client_updatelog))
                else:
                    self.update_log("Firmware not uploaded")
                    self.update_log_client("Firmware upload unsuccessful. Retrying again in a few minutes")
                    os.system(self.cmd_uploadLog_CPA)
                    os.system(self.cmd_uploadLog_Client)
                    self.flag_firmwarelog_upload = True

            # Upload logs to Drive
            if (loop_time - self.push_log_time) >= datetime.timedelta(hours=logging_time):
                if self.check_internet():
                    # print("Internet Connection")
                    print('Uploading logs to Drive')
                    self.push_log_time = datetime.datetime.now()
                    try:
                        os.system(logging_command)
                    except Exception as inst:
                        print(inst.args)
                        print(inst)

            # Upload Syslog to CPA
            if (loop_time - self.pull_syslog_time).days >= syslog_time:
                if self.check_internet():
                    # print("Internet Connection")
                    print('syslog sync')
                    self.pull_syslog_time = datetime.datetime.now()
                    pass

            # Check and reset internet every hour
            if (loop_time - self.time_network_reset).seconds >= 3600:
                if not self.check_internet():
                    os.system("sudo systemctl restart connman.service")
                    print("No internet. Restart connman")
                else:
                    print("There is internet. Connman restart not needed")
                self.time_network_reset = datetime.datetime.now()

            # Check to see if it is Monday and only do file handling after 1AM
            if datetime.date.today().weekday() == 0 and self.file_handling is False:
                if int(datetime.datetime.now().strftime("%H")) >= 1:
                    # Once a week remove updateslog file so that it doesn't get too big
                    if os.path.exists("/home/debian/updateslog.txt"):
                        os.remove("/home/debian/updateslog.txt")
                    try:
                        print("------- Starting File rotation and compression -------------")
                        files.folder_handling()
                        if files.folder_handling:
                            print("Folder handling complete")
                            self.file_handling = True
                    except Exception as inst:
                        print(inst)
            elif datetime.date.today().weekday() != 0 and self.file_handling is True:
                self.file_handling = False

            if int(datetime.datetime.now().strftime("%H")) == 1 and self.flag_RTCsync:
                self.flag_RTCsync = False

            # RTC sync with system clock at 00:00 every day
            if int(datetime.datetime.now().strftime("%H")) == 0 and not self.flag_RTCsync:
                print(" Starting RTC Sync ")
                os.system("sudo hwclock -w -f /dev/rtc1")

                time.sleep(10)

                var = subprocess.run(['sudo', 'hwclock', '-v'], capture_output=True, text=True).stdout
                #print(var)
                index1 = var.find("Time since last adjustment is") + 30
                index2 = var.find(" seconds\nCalculated Hardware Clock drift is")
                print(var[index1:index2])
                if int(var[index1:index2]) > 30:
                    self.flag_RTCsync = False
                    self.flag_RTCsync_error = True
                    print("RTC Flag is false")
                else:
                    self.flag_RTCsync = True
                    self.flag_RTCsync_error = False
                    print("RTC Flag is true")

            time.sleep(10)

    def check_update(self):
        """
        Determine if a firmware update is needed to be downloaded from the cloud
        :return:
        """
        try:
            with open(check_update_log, "r") as f:
                lines = f.read().splitlines()
                last_line = lines[-1]
                f.close()
            last_line = last_line.rsplit(": ")
            print(last_line)
            if int(last_line[-1][0]) > 0:
                self.flag_need_to_update = True
            elif int(last_line[-1][0]) == 0:
                self.flag_need_to_update = False
        except Exception as inst:
            print(type(inst))
            print(inst)

    def check_internet(self, host="8.8.8.8", port=53, timeout=2):
        """
        Check if there is internet connection.
        Code copied from: https://medium.com/better-programming/how-to-check-the-users-internet-connection-in-python-224e32d870c8
        :return: true/false
        """
        try:
            socket.setdefaulttimeout(timeout)
            socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
            return True
        except socket.error as ex:
            print(ex)
            return False

    def create_firmware_log(self, message):
        with open(os.path.join("/home/debian", self.CPA_updatelog), 'w') as file:
            file.writelines("Hash number: " + str(self.hash_num) + "  Firmware log status\n")
            file.writelines("Current firmware version: " + str(self.firmware_version) + "\n")
            file.writelines(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + " ---- " + message + "\n")
        file.close()
        with open(os.path.join("/home/debian", self.client_updatelog), 'w') as file:
            file.writelines("-------- Current firmware version: " + str(self.firmware_version) + " --------\n")
            file.writelines("New firmware detected. Firmware upgrade starting.\n")
        file.close()
        print("Log Files Created")

    def update_log(self, message):
        with open(os.path.join("/home/debian", self.CPA_updatelog), 'a') as file:
            file.writelines(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + " ---- " + message + "\n")
        file.close()

    def update_log_client(self, message):
        with open(os.path.join("/home/debian", self.client_updatelog), 'a') as file:
            file.writelines(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + " ---- " + message + "\n")
        file.close()
