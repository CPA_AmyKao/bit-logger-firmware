# import modules
import json
import struct

class batteryFormat:
    def __init__(self):
        self.upd_rack_data = []
        self.upd_mod_data = []
        self.config_setting = None
        self.load_config_file()
        self.flag_eventChange = False
        self.previous_eventAlarm = False
        self.previous_eventWarning = False
        self.previous_eventFault1 = False
        self.previous_eventFault2 = False

    def load_config_file(self):
        try:
            with open('/home/debian/BTL/Config/Product/LGChem_v1p9.json', 'r') as file:
                self.config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst)

    def format_battery_values(self, rack_data, mod_data):
        # Reset array every time new values are updated
        self.upd_rack_data.clear()
        self.upd_mod_data.clear()

        try:
            # Format rack data
            for i in range(len(self.config_setting["Rack"]["Scale"])):
                # First convert data type
                if self.config_setting["Rack"]["Data_type"][i] == "S_INT":
                    if rack_data[i] >= (self.config_setting["Singed_int_conversion"]/2):
                        rack_data[i] = rack_data[i] - self.config_setting["Singed_int_conversion"]
                elif self.config_setting["Rack"]["Data_type"][i] == "BIT":
                    rack_data[i] = format(rack_data[i], '016b')
                elif self.config_setting["Rack"]["Data_type"][i] == "BMS_Type":
                    rack_data[i] = self.config_setting["BMS_Type"][rack_data[i]]
                elif self.config_setting["Rack"]["Data_type"][i] == "Byte":
                    temp = struct.pack('>h', rack_data[i])
                    number = str(temp[0]) + '.' + str(temp[1])
                    number = int(number)
                    rack_data[i] = number

                # Scale the data
                if self.config_setting["Rack"]["Scale"][i] == 0:
                    self.upd_rack_data.append(rack_data[i])
                else:
                    self.upd_rack_data.append(rack_data[i] * self.config_setting["Rack"]["Scale"][i])


            # Format module data. 1st loop determines the number of modules to loop through
            for x in range(int(len(mod_data) / self.config_setting["Module"]["Count"])):
                offset = x * self.config_setting["Module"]["Count"]
                for i in range(len(self.config_setting["Module"]["Scale"])):
                    # First convert data type
                    if self.config_setting["Module"]["Data_type"][i] == "S_INT":
                        if mod_data[i + offset] >= (self.config_setting["Singed_int_conversion"] / 2):
                            mod_data[i + offset] = mod_data[i + offset] - self.config_setting["Singed_int_conversion"]
                    elif self.config_setting["Module"]["Data_type"][i] == "BIT":
                        mod_data[i + offset] = format(mod_data[i + offset], '016b')
                    elif self.config_setting["Module"]["Data_type"][i] == "BMS_Type":
                        mod_data[i + offset] = self.config_setting["BMS_Type"][str(mod_data[i + offset])]
                    elif self.config_setting["Module"]["Data_type"][i] == "Byte":
                        temp = struct.pack('>h', mod_data[i + offset])
                        number = str(temp[0]) + '.' + str(temp[1])
                        number = float(number)
                        mod_data[i + offset] = number

                    # Scale the data
                    if self.config_setting["Module"]["Scale"][i] == 0:
                        self.upd_mod_data.append(mod_data[i + offset])
                    else:
                        self.upd_mod_data.append(mod_data[i + offset] * self.config_setting["Module"]["Scale"][i])

            self.check_event()
        except Exception as inst:
            print(inst)
            return

    def check_event(self):
        current_event_alarm = False
        current_event_warning = False
        current_event_fault1 = False
        current_event_fault2 = False
        self.flag_eventChange = False
        if self.upd_rack_data[0] != 0:
            current_event_alarm = True
        if self.upd_rack_data[1] != 0:
            current_event_warning = True
        if self.upd_rack_data[2] != 0:
            current_event_fault1 = True
        if self.upd_rack_data[24] != 0:
            current_event_fault2 = True
        if self.previous_eventAlarm != current_event_alarm:
            self.flag_eventChange = True
            self.previous_eventAlarm = current_event_alarm
        if self.previous_eventWarning != current_event_warning:
            self.flag_eventChange = True
            self.previous_eventWarning = current_event_warning
        if self.previous_eventFault1 != current_event_fault1:
            self.flag_eventChange = True
            self.previous_eventFault1 = current_event_fault1
        if self.previous_eventFault2 != current_event_fault2:
            self.flag_eventChange = True
            self.previous_eventFault2 = current_event_fault2

    def get_rack_values(self):
        return self.upd_rack_data

    def get_mod_values(self):
        return self.upd_mod_data
