import os
import zipfile
import zlib
import time
import datetime


class FileRotation:
    def __init__(self):
        self.max_folder_size = 1000  # size in Mb
        self.size_of_delete = 50  # size in Mb, per chunk of data removed
        self.log_folder = "/home/debian/Logs"
        self.log_archive_folder = "/home/debian/Logs_archive"
        self.dict_files = dict()
        self.list_mdates = []
        self.file_rotation_complete = False
        # Check is folder exist, if not create a folder
        if not os.path.exists(self.log_archive_folder):
            os.mkdir(self.log_archive_folder)

    def get_folder_size(self, folder1, folder2):
        """
        Calculate total size of two specified folder and return size in MB
        """
        total_size = 0
        for path, dirs, files in os.walk(folder1):
            for f in files:
                fp = os.path.join(path, f)
                total_size += os.path.getsize(fp)
        for path, dirs, files in os.walk(folder2):
            for f in files:
                fp = os.path.join(path, f)
                total_size += os.path.getsize(fp)
        mod_total_size = total_size / 1024 ** 2  # Convert bytes to MB
        return mod_total_size

    def compress_files(self):
        # Generate a list of files that will be compressed
        self.list_files(self.log_folder, ".csv")

        # Find what year and date is today
        today = datetime.datetime.now()
        today_week = int(today.strftime("%W"))
        today_year = int(today.strftime("%Y"))

        # Select the compression mode ZIP_DEFLATED for compression
        # or zipfile.ZIP_STORED to just store the file
        compression = zipfile.ZIP_DEFLATED

        try:
            for names in self.list_mdates:
                if self.dict_files[names]["Year"] != today_year:
                    if today_week == 3:
                        zip_fname = str(self.dict_files[names]["Year"]) + str("_") + str(self.dict_files[names]["Week_num"]) + ".zip"
                        zf = zipfile.ZipFile(os.path.join(self.log_archive_folder, zip_fname), mode="a")
                        zf.write(self.dict_files[names]["filepath"],
                                 self.dict_files[names]["name"],
                                 compress_type=compression)
                        zf.close()
                        os.remove(self.dict_files[names]["filepath"])
                    elif today_week == 2:
                        if self.dict_files[names]["Week_num"] < 52:
                            zip_fname = str(self.dict_files[names]["Year"]) + str("_") + str(
                                self.dict_files[names]["Week_num"]) + ".zip"
                            zf = zipfile.ZipFile(os.path.join(self.log_archive_folder, zip_fname), mode="a")
                            zf.write(self.dict_files[names]["filepath"],
                                     self.dict_files[names]["name"],
                                     compress_type=compression)
                            zf.close()
                            os.remove(self.dict_files[names]["filepath"])
                else:
                    #print("Compressing files")
                    if self.dict_files[names]["Week_num"] < (today_week - 2):
                        zip_fname = str(self.dict_files[names]["Year"]) + str("_") + str(
                            self.dict_files[names]["Week_num"]) + ".zip"
                        zf = zipfile.ZipFile(os.path.join(self.log_archive_folder, zip_fname), mode="a")
                        zf.write(self.dict_files[names]["filepath"],
                                 self.dict_files[names]["name"],
                                 compress_type=compression)
                        zf.close()
                        os.remove(self.dict_files[names]["filepath"])
            self.file_rotation_complete = True
            #print("File compression complete")
        except Exception as inst:
            self.file_rotation_complete = False
            print(type(inst))
            print(inst)


    def remove_files(self):
        self.list_files(self.log_archive_folder, "zip")
        size = 0
        count = 0
        for i in range(len(self.list_mdates) - 1):
            size += (self.dict_files[self.list_mdates[i]]["size"] / (1024 ** 2))
            count += 1
            if size >= self.size_of_delete:
                #print("reached size of delete")
                break
        print("number of files to remove: " + str(count))
        try:
            for i in range(count):
                print("Removing" + self.dict_files[self.list_mdates[i]]["filepath"])
                os.remove(self.dict_files[self.list_mdates[i]]["filepath"])
        except Exception as inst:
            print(type(inst))
            print(inst)

    def list_files(self, folder, type):
        self.dict_files.clear()
        self.list_mdates.clear()
        for files in os.listdir(folder):
            if files.endswith(type):
                fp = os.path.join(folder, files)
                #print(fp)
                #print(files.rsplit('.', 1)[0])
                #print("-------------------------")
                ctime = datetime.datetime.fromtimestamp(os.stat(fp).st_ctime)
                week_num = int(ctime.strftime("%W"))
                year = int(ctime.strftime("%Y"))
                self.dict_files[files.rsplit('.', 1)[0]] = {"name": files, "size": os.path.getsize(fp), "filepath": fp, "Year": year, "Week_num": week_num}
        self.list_mdates = list(self.dict_files.keys())
        self.list_mdates.sort()

    def folder_handling(self):
        #print("Starting folder handling")
        # First archive past logs older then 2 weeks
        self.compress_files()
        #print("Finished file compression function")
        # Second, check total folder size does not exceed maximum specified size
        folder_size = self.get_folder_size(self.log_folder, self.log_archive_folder)
        print("Folder size: " + str(folder_size))

        # If folder size exceed maximum size, remove oldest archive files
        if folder_size >= self.max_folder_size:
            print("Size exceeded")
            try:
                self.remove_files()
                self.file_rotation_complete = True
            except Exception as inst:
                print(inst)
                self.file_rotation_complete = False

#file = FileRotation()
#file.folder_handling()
