# import modules
import can
from can import Message
import time
import json


class CanSender:
    def __init__(self):
        self.config_setting = None
        self.load_config_file()
        self.flag_alive = False
        self.flag_error = 0
        self.server = None
        self.init()

    def load_config_file(self):
        try:
            with open('/home/debian/BTL/Config/Product/Victron_can.json', 'r') as file:
                self.config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst.args)
            print(inst)
            return

    def init(self):
        try:
            self.server = can.interface.Bus(channel=self.config_setting["Connection"]["Channel"],
                                            bustype=self.config_setting["Connection"]["Bustype"],
                                            bitrate=self.config_setting["Connection"]["Bitrate"])
            self.server.flush_tx_buffer()
            self.flag_error = 0
            # self.check_alive()
            print('finished CAN channel initialisation')
        except Exception as inst:
            print(inst.args)
            print(inst)
            time.sleep(1)
            try:
                self.server = can.interface.Bus(channel=self.config_setting["Connection"]["Channel"],
                                                bustype=self.config_setting["Connection"]["Bustype"],
                                                bitrate=self.config_setting["Connection"]["Bitrate"])
                self.flag_error = 0
            except Exception as inst:
                print(inst.args)
                print(inst)
                self.flag_error = 1

    def check_alive(self):
        self.flag_alive = False
        for i in range(0, 1024):
            msg = self.server.recv(timeout=0.01)
            if msg is not None:
                print(str(msg.arbitration_id))
                if msg.arbitration_id == 773:
                    self.flag_alive = True
                    #print('Can Alive message received')
        if not self.flag_alive:
            self.flag_error = 2

    def send_msg(self, message):
        #self.check_alive()
        self.server.flush_tx_buffer()
        # print('sending message')
        #if self.flag_alive:
        try:
            for x, y in message.items():
                msg = Message(arbitration_id=x, data=y,
                              is_extended_id=self.config_setting["Msg_config"]["entended_id"])
                self.server.send(msg)
            self.flag_error = 0
        except Exception as inst:
            print(inst.args)
            print(inst)
            self.server.flush_tx_buffer()
