# import modules
import struct
import json


class Converter:
    def __init__(self):
        self.struc_format = ""
        self.client_config_setting = None
        self.server_config_setting = None
        self.load_config_file()
        self.temp_data = []
        self.converted_data = {}
        self.bytemsg = [0] * self.server_config_setting["Msg_config"]["bytes_per_msg"]
        self.alarm_data = []

    def load_config_file(self):

        try:
            with open('/home/debian/BTL/Config/Product/LGChem_v1p9.json', 'r') as file:
                self.client_config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst.args)
            print(inst)
            return
        try:
            with open('/home/debian/BTL/Config/Product/Victron_can.json', 'r') as file:
                self.server_config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst.args)
            print(inst)
            return
        self.format()

    def scale(self, data):
        self.temp_data.clear()
        for x, y in self.server_config_setting["Array_index"].items():
            if x == "Alarm":
                temp = data[y]
            else:
                temp = data[y] * self.server_config_setting["Scale"][x]
            self.temp_data.append(temp)

    def format(self):
        self.struc_format = ""
        if self.server_config_setting["Msg_config"]["Endian"] == "Little":
            self.struc_format = "<"
        elif self.server_config_setting["Msg_config"]["Endian"] == "Big":
            self.struc_format = ">"
        if self.server_config_setting["Msg_config"]["data_size"] == 1:
            self.struc_format = self.struc_format + "b"
        elif self.server_config_setting["Msg_config"]["data_size"] == 2:
            self.struc_format = self.struc_format + "h"
        elif self.server_config_setting["Msg_config"]["data_size"] == 4:
            self.struc_format = self.struc_format + "i"
        elif self.server_config_setting["Msg_config"]["data_size"] == 8:
            self.struc_format = self.struc_format + "q"

    def alarm_convert(self, data):
        self.alarm_data.clear()
        temp = data[int(self.server_config_setting["Array_index"]["Alarm"])]
        # If no alarm then get out of function
        if temp == 0:
            return
        # If all alarms (mean not polling data) raise all alarm flags
        if temp == self.server_config_setting["Alarm"]["All"]:
            self.alarm_data = [0x64, 0x41, 0x41, 0x01, 0, 0, 0, 0]
            self.temp_data[self.server_config_setting["Array_index"]["Alarm"]] = self.alarm_data.copy()
            return

        # Find position of the alarm ie find position of the 1's in the binanry data
        bits = []
        for i, c in enumerate(temp[:1:-1], 1):
            if c == '1':
                bits.append(i)
        # List the alarms triggered ie list what the corresponding bit in the position represents
        alarm_list = []
        for i in bits:
            for x, y in self.client_config_setting["Alarm_bit_position"].items():
                if i == y:
                    alarm_list.append(x)
        print(alarm_list)

        # Assign the alarm to the server side
        for i in range(self.server_config_setting["Msg_config"]["bytes_per_msg"] - 1):
            for x in alarm_list:
                if i == self.server_config_setting["Alarm"][x]["byte#"]:
                    # If one of the alarm is already set in the byte then add the alarm
                    if len(self.alarm_data) == i + 1:
                        self.alarm_data[i] = self.alarm_data[i] | self.server_config_setting["Alarm"][x]["bit#"]
                    else:
                        self.alarm_data.append(int(hex(self.server_config_setting["Alarm"][x]["bit#"]), base=16))
            self.alarm_data.append(0)

        # print(self.alarm_data)
        self.temp_data[self.server_config_setting["Array_index"]["Alarm"]] = self.alarm_data.copy()

    def convert(self, data, name):
        self.converted_data.clear()
        # Scale data
        if self.server_config_setting["Msg_config"]["Scale"]:
            # print('Scaling')
            self.scale(data)
            data.clear()
            data = self.temp_data.copy()
        # Convert alarm
        self.alarm_convert(data)
        data.clear()
        data = self.temp_data.copy()
        # Go through all the CAN ID and convert the data
        for x, y in self.server_config_setting["Identifier"].items():
            self.bytemsg.clear()
            self.bytemsg = [0] * self.server_config_setting["Msg_config"]["bytes_per_msg"]
            count = 0
            # Name or alarm deal seperately
            if y == "Name":
                x = int(x, base=16)
                if len(name) > self.server_config_setting["Msg_config"]["bytes_per_msg"]:
                    name = '%.8s' % name
                if len(name) < self.server_config_setting["Msg_config"]["bytes_per_msg"]:
                    for i in range(self.server_config_setting["Msg_config"]["bytes_per_msg"] - len(name)):
                        name = name + " "
                for char in name:
                    self.bytemsg[count] = int(hex(ord(char)), base=16)
                    count += 1
                self.converted_data.update({int(hex(x), base=16): self.bytemsg.copy()})

            elif y == "Alarm":
                x = int(x, base=16)
                self.converted_data.update(
                    {int(hex(x), base=16): data[self.server_config_setting["Array_index"]["Alarm"]]})

            else:
                # if there is more then 1 item within the ID to send
                if type(self.server_config_setting["Identifier"][x]) is list:
                    # Going through the list of what variables the
                    for i in range(len(self.server_config_setting["Identifier"][x])):
                        # Finding what variable is required
                        item = self.server_config_setting["Identifier"][x][i]
                        temp = tuple(struct.pack(self.struc_format, int(
                            data[self.server_config_setting["Array_index"][item]])))  # Convert data to required format
                        # If hex format is enabled
                        if self.server_config_setting["Msg_config"]["hex"]:
                            self.bytemsg[count] = int(hex(temp[0]), base=16)
                            self.bytemsg[count + 1] = int(hex(temp[1]), base=16)
                        else:
                            self.bytemsg[count] = int(temp[0])
                            self.bytemsg[count + 1] = int(temp[1])
                        count = count + 2
                    self.converted_data.update({int(x, base=16): self.bytemsg.copy()})
                else:
                    # Finding what variable is required
                    item = self.server_config_setting["Identifier"][x]
                    temp = tuple(
                        struct.pack(self.struc_format, int(data[self.server_config_setting["Array_index"][item]])))
                    # If hex format is enabled
                    if self.server_config_setting["Msg_config"]["hex"]:
                        self.bytemsg[count] = int(hex(temp[0]), base=16)
                        self.bytemsg[count + 1] = int(hex(temp[1]), base=16)
                    else:
                        self.bytemsg[count] = int(temp[0])
                        self.bytemsg[count + 1] = int(temp[1])
                    self.converted_data.update({int(x, base=16): self.bytemsg.copy()})

    def get_data(self):
        # print(self.converted_data)
        return self.converted_data
