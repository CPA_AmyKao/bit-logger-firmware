from flask import Flask, request, render_template, redirect, url_for, flash, Response, send_file
from flask_login import LoginManager, login_user, login_required, logout_user, UserMixin, current_user
from werkzeug.utils import secure_filename
from werkzeug.security import generate_password_hash, check_password_hash
import json
import os
import zipfile
import pathlib
import io
import time
import pexpect

firmware_version = 1.3

app = Flask(__name__)
app.config.update(
    SECRET_KEY='someting12345',
    UPLOAD_FOLDER=''
)

login_manager = LoginManager(app)
# flask-login
login_manager.init_app(app)

server_number_of_module = 0
server_rack_voltage = 0
server_rack_current = 0
server_rack_SOC = 0
internet_status = ''
dashboard = dict()
form_data = dict()
rclone = {"client_id": 'Default', "client_secret": 'Default', "verification_code": 'Default'}

Home_Folder = "/home/debian/BTL/"
Firmware_Folder = Home_Folder + "Firmware/"
User_config_file_tmp = Home_Folder + "Config/User/config_tmp.json"
User_config_file = Home_Folder + "Config/User/config.json"
User_pwd_file = Home_Folder + "Config/User/pwd.json"
logfile_folder = "/home/debian/Logs/"
log_folder = "/home/debian/Logs"
log_archive_folder = "/home/debian/Logs_archive"
RClone_config_folder = "/home/debian/.config/rclone/"
Root_RClone_config_folder = "/root/.config/rclone/"
Firmware_allowed_extensions = {'zip'}
RClone_allowed_extensions = {'conf'}
PASSWORD_ZIP = b'some_password'
check_filename = '0_Changelog'
wifi_config = "/var/lib/connman/wifi.config"
wifi_lines = ["[service_wifi_managed_psk]\n", "Type = wifi\n", "Name = \n", "SSID = \n", "Passphrase = \n"]

with open(User_pwd_file, 'r') as file:
    user_pwd = json.load(file)
file.close()
users = {user_pwd["username"]: {'password': user_pwd["password"]}}


# users = {'admin': {'password': 'admin12345'}, 'guest': {'password': 'guest'}}


def firmware_allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in Firmware_allowed_extensions


def rclone_allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in RClone_allowed_extensions


def format_for_dashboard():
    global dashboard, server_number_of_module, server_rack_voltage, server_rack_current, server_rack_SOC, internet_status

    dashboard['Number of modules'] = server_number_of_module
    dashboard['Average voltage (V)'] = server_rack_voltage
    dashboard['Current (A)'] = server_rack_current
    dashboard['SOC (%)'] = server_rack_SOC
    dashboard['Internet Available'] = internet_status


def server_update_values(battery_data, internet):
    global dashboard, server_number_of_module, server_rack_voltage, server_rack_current, server_rack_SOC, internet_status

    server_number_of_module = battery_data[0]
    server_rack_voltage = battery_data[1]
    server_rack_current = battery_data[2]
    server_rack_SOC = battery_data[3]
    internet_status = internet
    format_for_dashboard()


class User(UserMixin):
    pass


@login_manager.user_loader
def load_user(username):
    if username not in users:
        return None

    user = User()
    return user


@app.route('/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('status'))
    if request.method == 'POST':
        username = request.form["username"]
        if username == user_pwd["username"]:
            if request.form['password'] == users[username]['password']:
                user = User()
                user.id = username
                login_user(user)
                flash("Login successful")
                return redirect(url_for('status'))
            else:
                flash("Incorrect username or password")
                return redirect(url_for('login'))
        else:
            flash("Incorrect username or password")
            return redirect(url_for('login'))
    return render_template('login.html')


@app.route('/pwd_change', methods=['GET', 'POST'])
def password_change():
    global user_pwd
    if request.method == 'GET':
        return render_template('pwd_change.html', data=user_pwd["username"])
    elif request.method == 'POST':
        old_username = user_pwd["username"]
        if request.form['old_password'] == users[old_username]['password']:
            new_username = request.form['new_username']
            new_password = request.form['new_password']
            users[old_username]['password'] = new_password
            with open(User_pwd_file, 'r') as file:
                user_pwd = json.load(file)
            file.close()
            user_pwd["username"] = new_username
            user_pwd["password"] = new_password
            with open(User_pwd_file, 'w') as file:
                json.dump(user_pwd, file)
            file.close()
            flash("Change Successful!")
        else:
            flash("Old username or password incorrect. Please try again.")

        return render_template('pwd_change.html', data=user_pwd["username"])

    else:
        return render_template('pwd_change.html')


@app.route('/battery_status', methods=['GET', 'POST'])
@login_required
def status():
    global dashboard
    format_for_dashboard()
    if request.method == "POST":
        server_update_values()
        return redirect(url_for('refresh'))
    return render_template('battery_status.html', data=dashboard)


@app.route('/wifi', methods=['GET', 'POST'])
@login_required
def wifi_setup():
    if request.method == "GET":
        with open(wifi_config, 'r') as file:
            lines = file.readlines()
        file.close()
        flash(lines[2])
        flash(lines[3])
        flash(lines[4])
        return render_template('wifi.html')
    elif request.method == "POST":
        name = request.form.get("Name")
        ssid = request.form.get("SSID")
        passwd = request.form.get("Password")
        wifi_lines[2] = "Name = " + name + "\n"
        wifi_lines[3] = "SSID = " + ssid + "\n"
        wifi_lines[4] = "Passphrase = " + passwd + "\n"
        with open(wifi_config, 'w') as file:
            file.writelines(wifi_lines)
        file.close()
        flash("Wifi Added")
        return render_template('wifi.html')
    else:
        return render_template('wifi.html')


@app.route('/configuration', methods=['GET', 'POST'])
@login_required
def user_configuration():
    global form_data

    with open(User_config_file, 'r') as file:
        user_config = json.load(file)
    file.close()

    if request.method == "GET":

        return render_template('user_setup.html', data=user_config)
    elif request.method == 'POST':
        form_data.clear()
        form_data["Site_name"] = request.form.get("Site_name")
        form_data["Battery_slave_ID"] = request.form.get("Battery_slave_ID", type=int)
        form_data["Battery_number_of_modules"] = request.form.get("Battery_number_of_modules", type=int)
        form_data["Frequency_of_logs_sync"] = request.form.get("Frequency_of_logs_sync", type=float)
        form_data["Frequency_of_logging"] = request.form.get("Frequency_of_logging", type=int)
        form_data["Timezone"] = request.form.get("Timezone")
        form_data["Allow_remote_firmware_updates"] = request.form.get("list", type=bool)

        with open(User_config_file_tmp, 'w') as file:
            json.dump(form_data, file)
        file.close()
        os.rename(User_config_file_tmp, User_config_file)
        flash("Edit successful, please reboot after all setup completed for it to take affect")
        return render_template('user_setup.html', data=form_data)
    else:
        return redirect(url_for('status'))


@app.route('/rc-config', methods=['GET', 'POST'])
@login_required
def rclone_configuration_initial():
    if request.method == "POST":
        # Check if there is exsisting rclone configured for client. If yes, delete old client
        with open(os.path.join(Root_RClone_config_folder, 'rclone.conf'), 'r') as file:
            data = file.readlines()
        file.close()
        try:
            index = data.index('[Client_remote]\n')
            with open(os.path.join(Root_RClone_config_folder, 'rclone.conf'), 'w') as file:
                file.writelines(data[0:index])
            file.close()
        except ValueError:
            pass
        finally:
            rclone["client_id"] = request.form.get("client_id")
            rclone["client_secret"] = request.form.get("client_secret")
            # rclone["verification_code"] = request.form.get("verification_code")
            msg = configure_rclone(rclone["client_id"], rclone["client_secret"], b'default', 0)
            print(msg)
            flash(msg)
    else:
        return redirect(url_for('status'))
    return render_template('rc_config.html', data=rclone)


@app.route('/rc-configs', methods=['GET', 'POST'])
@login_required
def rclone_configuration_end():
    if request.method == "POST":
        rclone["client_id"] = request.form.get("client_id")
        rclone["client_secret"] = request.form.get("client_secret")
        rclone["verification_code"] = request.form.get("verification_code")
        msg = configure_rclone(rclone["client_id"], rclone["client_secret"], rclone["verification_code"], 1)
        print(msg)
        flash(msg)
        try:
            with open(os.path.join(Root_RClone_config_folder, 'rclone.conf'), 'r') as file:
                data = file.readlines()
            file.close()
            index = data.index('[Client_remote]\n')
            with open(os.path.join(RClone_config_folder, 'rclone.conf'), 'w') as file:
                file.writelines(data[index:])
            file.close()
            return send_file(os.path.join(RClone_config_folder, 'rclone.conf'), as_attachment=True)
        except:
            flash('Drive Configured but error with downloading config file')
    else:
        return redirect(url_for('status'))
    return render_template('rc_config.html', data=rclone)


def configure_rclone(client_id, client_secret, verification_code, count):
    child = pexpect.spawn('rclone config')
    fout = open(os.path.join(Home_Folder, 'rclonelog.txt'), 'wb')
    child.logfile = fout
    child.expect(['n/s/q> ', 'e/n/d/r/c/s/q> '])
    child.sendline('n')
    child.expect('name> ')
    child.sendline('Client_remote')
    child.expect('Storage> ')
    child.sendline('12')
    child.expect('client_id> ')
    child.sendline(client_id)
    child.expect('client_secret> ')
    child.sendline(client_secret)
    child.expect('scope> ')
    child.sendline('1')
    child.expect('root_folder_id> ')
    child.sendline('')
    child.expect('service_account_file> ')
    child.sendline('')
    child.expect('y/n> ')
    child.sendline('n')
    child.expect('y/n> ')
    child.sendline('n')
    if count == 0:
        child.read(size=70)
        child.sendcontrol('c')
        child.close()
        child.logfile.close()
        with open(os.path.join(Home_Folder, 'rclonelog.txt'), 'r') as file:
            data = file.readlines()
        file.close()
        start = data[-3].find('https:')
        link = data[-3][start:-1]
        return link
    else:
        child.expect('Enter verification code> ')
        child.sendline(verification_code)
        child.expect('y/n> ')
        child.sendline('n')
        child.expect('y/e/d> ')
        child.sendline('y')
        child.expect('e/n/d/r/c/s/q> ')
        child.sendline('q')
        child.close()
        child.logfile.close()
        return 'Drive configuration Complete'


@app.route('/rc-upload', methods=['GET', 'POST'])
@login_required
def rclone_upload():
    if request.method == "POST":
        if 'file' not in request.files:
            flash('No file part')
            return redirect(url_for('user_configuration'))

        uploaded_file = request.files['file']
        # if user does not select file, browser also submit an empty part without filename
        if uploaded_file.filename == '':
            flash("No selected file")
            return redirect(url_for('user_configuration'))

        if not rclone_allowed_file(uploaded_file.filename):
            flash("File not supported")
            return redirect(url_for('user_configuration'))
        if uploaded_file:
            filename = secure_filename(uploaded_file.filename)
            # print(filename)
            uploaded_file.save(os.path.join(RClone_config_folder, filename))
            flash("Upload Successful")
            # Check if there is exsisting rclone configured for client. If yes, delete old client
            with open(os.path.join(Root_RClone_config_folder, 'rclone.conf'), 'r') as file:
                data = file.readlines()
            file.close()
            try:
                index = data.index('[Client_remote]\n')
                with open(os.path.join(Root_RClone_config_folder, 'rclone.conf'), 'w') as file:
                    file.writelines(data[0:index])
                file.close()
            except ValueError:
                pass
            finally:
                try:
                    os.system("sudo cat /home/debian/.config/rclone/rclone.conf >> /root/.config/rclone/rclone.conf")
                    flash("Drive successfully added")
                    return redirect(url_for('status'))
                except Exception as inst:
                    print(inst.args)
                    print(inst)
                    flash("Drive Not added. Please try again")
                    return redirect(url_for('user_configuration'))
    else:
        return redirect(url_for('status'))
    # return render_template('rc_upload.html')


@app.route('/data_logs', methods=['GET', 'POST'])
@login_required
def log_files():
    if request.method == "GET":
        filename = list_files()
        return render_template('data_logs.html', data=filename)
    elif request.method == 'POST':
        filename = list_files()
        data = io.BytesIO()
        with zipfile.ZipFile(data, mode='w') as z:
            for x, y in filename.items():
                z.write(y, x)
        data.seek(0)
        return send_file(data, mimetype='application/zip', as_attachment=True, attachment_filename='logs.zip')



def list_files():
    # Endpoint to list files on the server
    files = dict()
    for filename in os.listdir(log_folder):
        path = os.path.join(log_folder, filename)
        if os.path.isfile(path):
            files[filename] = path
    for filename in os.listdir(log_archive_folder):
        path = os.path.join(log_archive_folder, filename)
        if os.path.isfile(path):
            files[filename] = path
    return files


@app.route('/firmware', methods=['GET', 'POST'])
@login_required
def update_firmware():
    global firmware_version, check_filename, PASSWORD_ZIP

    if request.method == 'POST':

        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(url_for('update_firmware'))

        uploaded_file = request.files['file']
        # if user does not select file, browser also submit an empty part without filename
        if uploaded_file.filename == '':
            flash("No selected file")
            return redirect(url_for('update_firmware'))

        if not firmware_allowed_file(uploaded_file.filename):
            flash("File not supported")
            return redirect(url_for('update_firmware'))

        if uploaded_file:
            filename = secure_filename(uploaded_file.filename)
            print(filename)
            flash("Uploading file:", uploaded_file.filename)
            uploaded_file.save(os.path.join(Firmware_Folder, filename))
            print(os.path.join(Firmware_Folder, filename))
            try:
                zip_ref = zipfile.ZipFile(os.path.join(Firmware_Folder, filename), 'r')
                print("Zip opened")
                for x in zip_ref.namelist():
                    if x == check_filename:
                        print("Found required file")
                        print(PASSWORD_ZIP)
                        zip_ref.extractall(path=Home_Folder, pwd=PASSWORD_ZIP)

                        # zip_ref.extractall(os.path.join(Firmware_Folder, 'testing'), pwd=PASSWORD_ZIP)
                        print("Extracted file")
                        zip_ref.close()
                        os.remove(os.path.join(Firmware_Folder, filename))
                        flash("Software uploaded! Restart to take affect...")
                        return redirect(url_for('update_firmware'))
                flash("Not CPA firmware, please re-upload correct firmware")
                zip_ref.close()
                os.remove(os.path.join(Firmware_Folder, filename))
                return redirect(url_for('update_firmware'))
            except Exception as inst:
                print(inst.args)
                print(inst)
                flash("ERROR: Upload file: Unzip error")
                os.remove(os.path.join(Firmware_Folder, filename))
                return redirect(url_for('update_firmware'))

    elif request.method == "GET":
        return render_template('firmware.html', data=firmware_version)
    else:
        return redirect(url_for('status'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("Logout successful")
    return redirect(url_for('login'))


@app.route('/reboot')
def reboot():
    os.system('sudo reboot')
    return redirect(url_for('logout'))


# handle login failed
@app.errorhandler(401)
@app.errorhandler(400)
def page_not_found(e):
    flash("Server: Error 401 handled")
    return redirect(url_for('status'))


@login_manager.unauthorized_handler
def unauthorized_handler():
    flash("Unauthorised. Please login first.")
    return redirect(url_for('login'))


def translator_server():
    print("Server: Starting server...")
    while True:
        try:
            app.run(host='0.0.0.0', port=5000, debug=False, threaded=True, use_reloader=False)
            print("Server thread")
        except:
            print("Server ERROR: Unable to start server, port may be in use, try again in 5 seconds...")

        time.sleep(5)
