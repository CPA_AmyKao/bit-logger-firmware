# import modules
import datetime
import json


class Controller:
    def __init__(self):
        self.client_config_setting = None
        self.server_config_setting = None
        self.load_config_file()
        self.controller_data = []
        self.current_time = datetime.datetime.now()

    def load_config_file(self):
        try:
            with open('/home/debian/BTL/Config/Product/LGChem_v1p9.json', 'r') as file:
                self.client_config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst)
            return
        try:
            with open('/home/debian/BTL/Config/Product/Victron_can.json', 'r') as file:
                self.server_config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst)
            return

    def add_charging_values(self):
        """
        Add Max volts and Min volts to the controller data array
        :return: None
        """
        self.controller_data.append(self.client_config_setting["Charge_setting"]["Max_volt"])
        self.controller_data.append(self.client_config_setting["Charge_setting"]["Min_volt"])

    def charging_control(self, data, error_flag, time):
        self.controller_data.clear()
        self.controller_data = data.copy()
        self.add_charging_values()

        # No battery error flag
        if error_flag == 0:
            # CCL = 0 if SOC>90 or Volt > 57.4 or if SOC<10
            if self.controller_data[self.server_config_setting["Array_index"]["SOC"]] >= \
                    self.client_config_setting["Charge_setting"]["Max_SOC"] or self.controller_data[
                self.server_config_setting["Array_index"]["Volt"]] >= self.client_config_setting["Charge_setting"][
                "Max_volt"]:
                self.controller_data[self.server_config_setting["Array_index"]["CCL"]] = 0
            if self.controller_data[self.server_config_setting["Array_index"]["SOC"]] <= \
                    self.client_config_setting["Charge_setting"]["Min_SOC"]:
                self.controller_data[self.server_config_setting["Array_index"]["CCL"]] = 0
            return

        # No battery data
        else:
            self.current_time = datetime.datetime.now()
            time_difference = self.current_time - time
            if time_difference.seconds > 60:
                self.controller_data[self.server_config_setting["Array_index"]["Alarm"]] = \
                self.server_config_setting["Alarm"]["All"]  # Raise all alarm flag on Server side
                for i in range(1, self.server_config_setting["Array_index"]["CCL"]):
                    self.controller_data[i] = 0
            else:
                self.controller_data[self.server_config_setting["Array_index"]["CCL"]] = \
                self.client_config_setting["Charge_setting"]["CCL"]
                self.controller_data[self.server_config_setting["Array_index"]["DCL"]] = \
                self.client_config_setting["Charge_setting"]["DCL"]
                self.controller_data[self.server_config_setting["Array_index"]["Max_volt"]] = \
                self.client_config_setting["Charge_setting"]["Max_volt"]
                self.controller_data[self.server_config_setting["Array_index"]["Min_volt"]] = \
                self.client_config_setting["Charge_setting"]["Min_volt"]
            return

    def get_data(self):
        #print(self.controller_data)
        return self.controller_data
