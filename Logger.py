# import modules
import csv
import datetime
import os.path
from os import path
import json


class DataLogging:
    def __init__(self, num_of_module):
        self.num_of_module = num_of_module
        self.config_setting = None
        self.load_config_file()
        self.old_timestr = datetime.datetime.today().strftime('%Y%m%d')  # Used to keep time of when files were created
        self.new_timestr = datetime.datetime.today().strftime('%Y%m%d')  # Time now
        self.path = '/home/debian/Logs'
        self.filename = self.new_timestr + '.csv'
        self.file_status = False  # Default setting with no file and a new file needs to be created
        self.check_file_exist()
        self.counter = 0
        self.create_new_file()
        self.timestamp = datetime.datetime.now() - datetime.timedelta(
            seconds=200)  # Set a default time so the first data entries will be logged

    def load_config_file(self):
        try:
            with open('/home/debian/BTL/Config/Product/LGChem_v1p9.json', 'r') as file:
                self.config_setting = json.load(file)
            file.close()
        except Exception as inst:
            print(inst)

    def check_file_exist(self):
        self.file_status = path.exists(os.path.join(self.path, self.filename))

    def create_new_file(self):
        """
        If no file exist in the predefined directory with the specified filename, create a new CSV file with the headers populated
        :return:
        """
        if not self.file_status:
            self.old_timestr = datetime.datetime.today().strftime('%Y%m%d')  # Save the time when the file was created
            self.counter = 0  # Reset counter
            with open(os.path.join(self.path, self.filename), 'a+', newline='') as csvfile:
                writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                mod_temp = []
                for i in range(self.num_of_module):
                    mod_temp = mod_temp + ["Module " + str(i + 1)] * self.config_setting["Module"]["Count"]
                header = ['Counter', 'Timestamp', 'BitLogger_Error'] + (
                        ["Rack"] * self.config_setting["Rack"]["Count"]) + mod_temp
                writer.writerow(header)
                header = ['Counter', 'Timestamp', 'BitLogger_Error'] + self.config_setting["Rack"][
                    "Header"] + self.config_setting["Module"]["Header"] * self.num_of_module
                writer.writerow(header)

            csvfile.close()
        else:
            self.check_last_counter()

    def log_data(self, rack, mod, bitLogger_error):
        self.check_new_day()
        self.timestamp = datetime.datetime.now()

        if 0 < bitLogger_error <= 3:
            temp = [self.counter, self.timestamp.strftime('%Y-%m-%d %H:%M:%S'), format(bitLogger_error, '016b')] + [
                'N/A'] * (self.config_setting["Rack"]["Count"] + self.config_setting["Module"][
                "Count"] * self.num_of_module)
        else:
            temp = [self.counter, self.timestamp.strftime('%Y-%m-%d %H:%M:%S'), format(bitLogger_error, '016b')] + rack + mod
        self.counter += 1

        with open(os.path.join(self.path, self.filename), 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(temp)
        csvfile.close()

    def check_new_day(self):
        # If time now is not the same as when a file was created then create new file
        self.new_timestr = datetime.datetime.today().strftime('%Y%m%d')
        if self.new_timestr != self.old_timestr:
            self.filename = self.new_timestr + '.csv'
            self.check_file_exist()
            self.create_new_file()

    def check_last_counter(self):
        try:
            # Read the last row first element where counter is situated
            #print(os.path.join(self.path, self.filename))
            with open(os.path.join(self.path, self.filename), 'r', newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                #print(reader)
                for row in reader:
                    lines = row[:-1][0]
            csvfile.close()
            if lines != 'Counter':
                self.counter = int(lines) + 1
        except Exception as inst:
            print(inst)
